#!/bin/bash

# Clean all
for dir in BUILD BUILDROOT RPMS SOURCES SPECS SRPMS
do
  [[ -d $dir ]] && rm -Rf $dir
  mkdir -p $dir
done

echo "Name:           libvmod-digest
Version:        1.0.2
Release:        1%{?dist}
Summary:        Varnish Digest Module

Vendor:         Kristian Lyngstøl
Group:          System Environment/Libraries
License:        BSD
URL:            https://github.com/varnish/libvmod-digest
Source0:        https://github.com/varnish/libvmod-digest/archive/master.zip
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  varnish-libs-devel
BuildRequires:  mhash-devel
Requires:       varnish-libs
Requires:       mhash
Requires:       python-docutils

%description
Varnish Module (vmod) for computing HMAC, message digests and working with base64.
All HMAC- and hash-functionality is provided by libmhash, while base64 is implemented locally.

%prep
%setup -q -n libvmod-digest-master

%build
./autogen.sh
./configure
make %{?_smp_mflags}

%install
install --directory %{buildroot}/usr/lib64/varnish/vmods
install -m 0755 src/.libs/libvmod_digest.so %{buildroot}/usr/lib64/varnish/vmods

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%config(noreplace) /usr/lib64/varnish/vmods/libvmod_digest.so" > SPECS/libvmod-digest.spec

wget https://github.com/varnish/libvmod-digest/archive/master.zip -O SOURCES/master.zip
rpmbuild --define '_topdir '$(pwd) -ba SPECS/libvmod-digest.spec
