#!/bin/bash

NAME="nsca-ng"
VERSION="1.5"

# Clean all
for dir in BUILD BUILDROOT RPMS SOURCES SPECS SRPMS
do
  [[ -d $dir ]] && rm -Rf $dir
  mkdir -p $dir
done

echo "#
%if 0%{?rhel_version} > 6 || 0%{?centos_version} > 6 || 0%{?fedora_version} >= 20 || 0%{?el7}%{?fc20}%{?fc21}%{?fc22}
%bcond_without systemd
%else
%bcond_with    systemd
%endif

Name:           ${NAME}
Version:        ${VERSION}
Release:        0%{?dist}
License:        BSD-2-Clause
Summary:        A modern replacement for NSCA
Url:            https://www.nsca-ng.org/
Group:          System/Monitoring
Source:         https://www.nsca-ng.org/download/nsca-ng-%{version}.tar.gz
BuildRequires:  libconfuse-devel
BuildRequires:  libev-devel
BuildRequires:  openssl-devel >= 1.0.0
BuildRequires:  pkgconfig
%if %{with systemd}
BuildRequires:  pkgconfig(systemd)
%endif
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
%description
The NSCA-ng package provides a client-server pair which makes the \"Nagios
command file\" accessible to remote systems.  This allows for submitting
passive check results, downtimes, and many other commands to Nagios (or
compatible monitoring solutions).

NSCA-ng supports TLS encryption and shared-secret authentication with
client-specific passwords (based on RFC 4279), as well as fine-grained
authorization control.

%package server
Group:          System/Monitoring
%if %{with systemd}
%{?systemd_requires}
%endif
#
Summary:        NSCA-ng server
%description server
NSCA-ng provides a client-server pair that makes the Nagios command file accessible to remote systems.

This is the server component of NSCA-ng.

%package client
Group:          System/Monitoring
#
Summary:        NSCA-ng client
%description client
NSCA-ng provides a client-server pair that makes the Nagios command file accessible to remote systems.

This is the client component of NSCA-ng.

%prep
%setup -q

%build
export CFLAGS=\"%{optflags} -fno-strict-aliasing\"
%configure --enable-server --disable-silent-rules
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot} %{?_smp_mflags}
%if %{with systemd}
install -D -m 0644 etc/%{name}.service %{buildroot}%{_unitdir}/%{name}.service
install -D -m 0644 etc/%{name}.socket  %{buildroot}%{_unitdir}/%{name}.socket
%endif

# START BIG SYSTEMD
%if %{with systemd}
%preun server
%systemd_preun %{name}.service
%systemd_preun %{name}.socket

%post server
%systemd_post %{name}.service
%systemd_post %{name}.socket

%postun server
%systemd_postun_with_restart %{name}.service
%systemd_postun_with_restart %{name}.socket
%endif
# /END BIG SYSTEMD

%files server
%defattr(-,root,root)
%doc ChangeLog README COPYING
%config(noreplace) %attr(0640,root,nagios) %{_sysconfdir}/nsca-ng.cfg
%{_sbindir}/nsca-ng
%{_mandir}/man5/nsca-ng.cfg.5.gz
%{_mandir}/man8/nsca-ng.8.gz
%if %{with systemd}
%{_unitdir}/%{name}.service
%{_unitdir}/%{name}.socket
%endif

%files client
%defattr(-,root,root)
%config(noreplace) %attr(0640,root,nagios) %{_sysconfdir}/send_nsca.cfg
%{_sbindir}/send_nsca
%{_mandir}/man5/send_nsca.cfg.5.gz
%{_mandir}/man8/send_nsca.8.gz" > SPECS/nsca-ng.spec

wget https://www.${NAME}.org/download/${NAME}-${VERSION}.tar.gz -O SOURCES/${NAME}-${VERSION}.tar.gz
rpmbuild --define '_topdir '$(pwd) -ba SPECS/nsca-ng.spec
