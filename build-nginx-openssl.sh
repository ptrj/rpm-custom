#!/bin/bash

CENTVER="6"
OPENSSL="openssl-1.0.2t"
NGINX="nginx-1.14.2-1"

## Check if:
#yum clean all
## Install epel packages (required for GeoIP-devel)
#yum -y install epel-release
#yum -y groupinstall 'Development Tools'
#yum -y install wget openssl-devel libxml2-devel libxslt-devel gd-devel perl-ExtUtils-Embed GeoIP-devel pcre-devel
#useradd builder
#groupadd builder

# Clean up and create directories
for dir in BUILD RPMS SOURCES SPECS SRPMS LIB
do
 [[ -d $dir ]] && rm -Rf $dir
  mkdir $dir
done

# Untar, but don't compile openssl to ./LIB
wget https://www.openssl.org/source/$OPENSSL.tar.gz -O LIB/$OPENSSL.tar.gz
tar -zxvf LIB/open* -C LIB

# Build source nginx (no auto-updates), statically link to ./LIB/openssl* (no OS effects)
wget http://nginx.org/packages/centos/$CENTVER/SRPMS/$NGINX.el$CENTVER.ngx.src.rpm -O SOURCES/$NGINX.el$CENTVER.ngx.src.rpm
cd SOURCES && \
rpm2cpio $NGINX.el$CENTVER.ngx.src.rpm | cpio -idmv && \
sed -i "s|--with-http_ssl_module|--with-http_ssl_module --with-openssl=$(pwd)/../LIB/$OPENSSL|g" nginx.spec && \
sed -i "s|%define WITH_LD_OPT .*|%define WITH_LD_OPT \"\"|g" nginx.spec && \
sed -i "s| -fPIC||g" nginx.spec && \
cp nginx.spec ../SPECS/nginx.spec && \
cd ../

# Compile it
rpmbuild --define '_topdir '$(pwd) -ba SPECS/nginx.spec
