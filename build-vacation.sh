#!/bin/bash

NAME=vacation
VERSION=1.2.8.0-beta1

# Clean all
for dir in BUILD BUILDROOT RPMS SOURCES SPECS SRPMS
do
  [[ -d $dir ]] && rm -Rf $dir
  mkdir -p $dir
done

echo "Name:           vacation
Version:        1.2.8.0
Release:        1.beta1%{?dist}
Summary:        Reply to mail automatically

Vendor:         Chris Samuel & Sean Rima
License:        GPL
URL:            http://www.csamuel.org/software/vacation/
Source:         http://sourceforge.net/projects/%{name}/files/%{name}/%{version}-beta1/%{name}-%{version}-beta1.tar.gz

BuildRequires:  gdbm-devel
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
email autoresponder

%prep
%setup -q -n %{name}-%{version}-beta1
%build

%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR="%{buildroot}"

%clean
%{__rm} -rf %{buildroot}

%files
%define _unpackaged_files_terminate_build 0
%defattr(-, root, root, 0755)
%{_bindir}/vacation
%{_bindir}/vaclook
%doc README AUTHORS COPYING TODO ChangeLog
%doc %{_mandir}/man1/vacation.1.gz
%doc %{_mandir}/man1/vaclook.1.gz
%doc %{_mandir}/de/man1/vacation.1.gz" > SPECS/vacation.spec

wget http://sourceforge.net/projects/$NAME/files/$NAME/$VERSION/$NAME-$VERSION.tar.gz -O SOURCES/$NAME-$VERSION.tar.gz
rpmbuild --define '_topdir '$(pwd) -ba SPECS/vacation.spec
