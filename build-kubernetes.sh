#!/bin/bash

NAME=kubernetes
VERSION=1.5.7

## Clean all
for dir in BUILD BUILDROOT RPMS SOURCES SPECS SRPMS TMP
do
  [[ -d $dir ]] && rm -Rf $dir
  mkdir -p $dir
done

## Download kubernetes binaries
wget https://github.com/$NAME/$NAME/releases/download/v$VERSION/$NAME.tar.gz -O TMP/$NAME-$VERSION.tar.gz && \
tar xzf TMP/$NAME-$VERSION.tar.gz -C TMP/ && \
yes | TMP/kubernetes/cluster/get-kube-binaries.sh && \
tar --strip-components=3 -C SOURCES/ -vxzf TMP/kubernetes/server/kubernetes-server-linux-amd64.tar.gz kubernetes/server/bin && \
rm -rf TMP

## Systemd kube-apiserver
echo "[Unit]
Description=Kubernetes API Server
Documentation=https://github.com/GoogleCloudPlatform/kubernetes
After=network.target
After=etcd.service

[Service]
EnvironmentFile=-/etc/kubernetes/config
EnvironmentFile=-/etc/kubernetes/apiserver
User=kube
ExecStart=/usr/bin/kube-apiserver \\
            \$KUBE_LOGTOSTDERR \\
            \$KUBE_LOG_LEVEL \\
            \$KUBE_ETCD_SERVERS \\
            \$KUBE_API_ADDRESS \\
            \$KUBE_API_PORT \\
            \$KUBELET_PORT \\
            \$KUBE_ALLOW_PRIV \\
            \$KUBE_SERVICE_ADDRESSES \\
            \$KUBE_ADMISSION_CONTROL \\
            \$KUBE_API_ARGS
Restart=on-failure
Type=notify
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target" > SOURCES/kube-apiserver.service

## Systemd kube-controll-manager
echo "[Unit]
Description=Kubernetes Controller Manager
Documentation=https://github.com/GoogleCloudPlatform/kubernetes

[Service]
EnvironmentFile=-/etc/kubernetes/config
EnvironmentFile=-/etc/kubernetes/controller-manager
User=kube
ExecStart=/usr/bin/kube-controller-manager \\
            \$KUBE_LOGTOSTDERR \\
            \$KUBE_LOG_LEVEL \\
            \$KUBE_MASTER \\
            \$KUBE_CONTROLLER_MANAGER_ARGS
Restart=on-failure
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target" > SOURCES/kube-controller-manager.service

## Systemd kube-scheduler
echo "[Unit]
Description=Kubernetes Scheduler Plugin
Documentation=https://github.com/GoogleCloudPlatform/kubernetes

[Service]
EnvironmentFile=-/etc/kubernetes/config
EnvironmentFile=-/etc/kubernetes/scheduler
User=kube
ExecStart=/usr/bin/kube-scheduler \\
            \$KUBE_LOGTOSTDERR \\
            \$KUBE_LOG_LEVEL \\
            \$KUBE_MASTER \\
            \$KUBE_SCHEDULER_ARGS
Restart=on-failure
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target" > SOURCES/kube-scheduler.service

## Systemd kube-proxy
echo "[Unit]
Description=Kubernetes Kube-Proxy Server
Documentation=https://github.com/GoogleCloudPlatform/kubernetes
After=network.target

[Service]
EnvironmentFile=-/etc/kubernetes/config
EnvironmentFile=-/etc/kubernetes/proxy
ExecStart=/usr/bin/kube-proxy \\
            \$KUBE_LOGTOSTDERR \\
            \$KUBE_LOG_LEVEL \\
            \$KUBE_MASTER \\
            \$KUBE_PROXY_ARGS
Restart=on-failure
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target" > SOURCES/kube-proxy.service

## Systemd kubelet
echo "[Unit]
Description=Kubernetes Kubelet Server
Documentation=https://github.com/GoogleCloudPlatform/kubernetes
After=docker.service
Requires=docker.service

[Service]
WorkingDirectory=/var/lib/kubelet
EnvironmentFile=-/etc/kubernetes/config
EnvironmentFile=-/etc/kubernetes/kubelet
ExecStart=/usr/bin/kubelet \\
            \$KUBE_LOGTOSTDERR \\
            \$KUBE_LOG_LEVEL \\
            \$KUBELET_API_SERVER \\
            \$KUBELET_ADDRESS \\
            \$KUBELET_PORT \\
            \$KUBELET_HOSTNAME \\
            \$KUBE_ALLOW_PRIV \\
            \$KUBELET_POD_INFRA_CONTAINER \\
            \$KUBELET_ARGS
Restart=on-failure

[Install]
WantedBy=multi-user.target" > SOURCES/kubelet.service

## Systemd accounting
echo "[Manager]
DefaultCPUAccounting=yes
DefaultMemoryAccounting=yes
" > SOURCES/kubernetes-accounting.conf

## Default Kube Global Config
echo "###
# kubernetes system config
#
# The following values are used to configure various aspects of all
# kubernetes services, including
#
#   kube-apiserver.service
#   kube-controller-manager.service
#   kube-scheduler.service
#   kubelet.service
#   kube-proxy.service
# logging to stderr means we get it in the systemd journal
KUBE_LOGTOSTDERR=\"--logtostderr=true\"

# journal message level, 0 is debug
KUBE_LOG_LEVEL=\"--v=0\"

# Should this cluster be allowed to run privileged docker containers
KUBE_ALLOW_PRIV=\"--allow-privileged=false\"

# How the controller-manager, scheduler, and proxy find the apiserver
KUBE_MASTER=\"--master=http://127.0.0.1:8080\"" > SOURCES/config.conf

## Default Kube Apiserver Config
echo "###
# kubernetes system config
#
# The following values are used to configure the kube-apiserver
#

# The address on the local server to listen to.
KUBE_API_ADDRESS=\"--insecure-bind-address=127.0.0.1\"

# The port on the local server to listen on.
# KUBE_API_PORT=\"--port=8080\"

# Port minions listen on
# KUBELET_PORT=\"--kubelet-port=10250\"

# Comma separated list of nodes in the etcd cluster
KUBE_ETCD_SERVERS=\"--etcd-servers=http://127.0.0.1:2379\"

# Address range to use for services
KUBE_SERVICE_ADDRESSES=\"--service-cluster-ip-range=10.254.0.0/16\"

# default admission control policies
KUBE_ADMISSION_CONTROL=\"--admission-control=NamespaceLifecycle,NamespaceExists,LimitRanger,SecurityContextDeny,ServiceAccount,ResourceQuota\"

# Add your own!
KUBE_API_ARGS=\"\"" > SOURCES/apiserver.conf

## Default Kube Controller Manager Config
echo "###
# The following values are used to configure the kubernetes controller-manager

# defaults from config and apiserver should be adequate

# Add your own!
KUBE_CONTROLLER_MANAGER_ARGS=\"\"" > SOURCES/controller-manager.conf

## Default Kube Scheduler Config
echo "###
# kubernetes scheduler config

# default config should be adequate

# Add your own!
KUBE_SCHEDULER_ARGS=\"\"" > SOURCES/scheduler.conf

## Default Kube Proxy Config
echo "###
# kubernetes proxy config

# default config should be adequate

# Add your own!
KUBE_PROXY_ARGS=\"\"" > SOURCES/proxy.conf

## Default Kubelet Config
echo "###
# kubernetes kubelet (minion) config

# The address for the info server to serve on (set to 0.0.0.0 or "" for all interfaces)
KUBELET_ADDRESS=\"--address=127.0.0.1\"

# The port for the info server to serve on
# KUBELET_PORT=\"--port=10250\"

# You may leave this blank to use the actual hostname
KUBELET_HOSTNAME=\"--hostname-override=127.0.0.1\"

# location of the api-server
KUBELET_API_SERVER=\"--api-servers=http://127.0.0.1:8080\"

# pod infrastructure container
KUBELET_POD_INFRA_CONTAINER=\"--pod-infra-container-image=registry.access.redhat.com/rhel7/pod-infrastructure:latest\"

# Add your own!
KUBELET_ARGS=\"\"" > SOURCES/kubelet.conf

## tmpfiles
echo "d /var/run/kubernetes 0755 kube kube -" > SOURCES/kubernetes.conf

echo "Name:           kubernetes
Version:        ${VERSION}
Release:        1%{?dist}
Summary:        Container cluster management

Vendor:         Google Inc.
License:        ASL 2.0
URL:            https://github.com/%{name}/%{name}
Source0:        kube-apiserver
Source1:        kube-scheduler
Source2:        kube-controller-manager
Source3:        kube-proxy
Source4:        kubelet
Source5:        kubectl
Source6:        kube-apiserver.service
Source7:        kube-controller-manager.service
Source8:        kube-scheduler.service
Source9:        kube-proxy.service
Source10:       kubelet.service
Source11:       kubernetes-accounting.conf
Source12:       config.conf
Source13:       apiserver.conf
Source14:       controller-manager.conf
Source15:       scheduler.conf
Source16:       proxy.conf
Source17:       kubelet.conf
Source18:       kubernetes.conf

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: kubernetes-master = %{version}-%{release}
Requires: kubernetes-node = %{version}-%{release}

%description
Container cluster management

%package master
Summary: Kubernetes services for master host

Requires: kubernetes-client = %{version}-%{release}

Conflicts: kubernetes-node < %{version}-%{release}
Conflicts: kubernetes-node > %{version}-%{release}

%description master
Kubernetes services for master host

%package node
Summary: Kubernetes services for node host

Requires: socat
Requires: conntrack-tools
Requires: kubernetes-client = %{version}-%{release}

Conflicts: kubernetes-master < %{version}-%{release}
Conflicts: kubernetes-master > %{version}-%{release}

%description node
Kubernetes services for node host

%package client
Summary: Kubernetes client tools

%description client
Kubernetes client tools like kubectl

%setup -n %{name}-%{version}
%build
%install
install -d -m 0755 %{buildroot}%{_sysconfdir}/%{name}
install -m 0644 %{SOURCE12} %{buildroot}%{_sysconfdir}/%{name}/config
install -m 0644 %{SOURCE13} %{buildroot}%{_sysconfdir}/%{name}/apiserver
install -m 0644 %{SOURCE14} %{buildroot}%{_sysconfdir}/%{name}/controller-manager
install -m 0644 %{SOURCE15} %{buildroot}%{_sysconfdir}/%{name}/scheduler
install -m 0644 %{SOURCE16} %{buildroot}%{_sysconfdir}/%{name}/proxy
install -m 0644 %{SOURCE17} %{buildroot}%{_sysconfdir}/%{name}/kubelet
install -d -m 0755 %{buildroot}%{_sysconfdir}/systemd/system.conf.d
install -p -m 0644 %{SOURCE11} %{buildroot}%{_sysconfdir}/systemd/system.conf.d
mkdir -p %{buildroot}/run
install -d -m 0755 %{buildroot}/run/%{name}
install -m 755 -d %{buildroot}%{_bindir}
install -m 755 %{SOURCE0} %{buildroot}%{_bindir}
install -m 755 %{SOURCE1} %{buildroot}%{_bindir}
install -m 755 %{SOURCE2} %{buildroot}%{_bindir}
install -m 755 %{SOURCE3} %{buildroot}%{_bindir}
install -m 755 %{SOURCE4} %{buildroot}%{_bindir}
install -m 755 %{SOURCE5} %{buildroot}%{_bindir}
install -d -m 0755 %{buildroot}%{_unitdir}
install -m 644 %{SOURCE6} %{buildroot}%{_unitdir}
install -m 644 %{SOURCE7} %{buildroot}%{_unitdir}
install -m 644 %{SOURCE8} %{buildroot}%{_unitdir}
install -m 644 %{SOURCE9} %{buildroot}%{_unitdir}
install -m 644 %{SOURCE10} %{buildroot}%{_unitdir}
install -d -m 0775 %{buildroot}%{_tmpfilesdir}
install -m 644 %{SOURCE18} %{buildroot}%{_tmpfilesdir}
install -d %{buildroot}%{_sharedstatedir}/kubelet

%files
# empty

%files master
%attr(754, -, kube) %caps(cap_net_bind_service=ep) %{_bindir}/kube-apiserver
%{_bindir}/kube-controller-manager
%{_bindir}/kube-scheduler
%{_unitdir}/kube-apiserver.service
%{_unitdir}/kube-controller-manager.service
%{_unitdir}/kube-scheduler.service
%dir %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/apiserver
%config(noreplace) %{_sysconfdir}/%{name}/scheduler
%config(noreplace) %{_sysconfdir}/%{name}/config
%config(noreplace) %{_sysconfdir}/%{name}/controller-manager
%{_tmpfilesdir}/kubernetes.conf
%verify(not size mtime md5) %attr(755, kube,kube) %dir /run/%{name}

%files node
%{_bindir}/kubelet
%{_bindir}/kube-proxy
%{_unitdir}/kubelet.service
%{_unitdir}/kube-proxy.service
%dir %{_sharedstatedir}/kubelet
%dir %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/config
%config(noreplace) %{_sysconfdir}/%{name}/kubelet
%config(noreplace) %{_sysconfdir}/%{name}/proxy
%config(noreplace) %{_sysconfdir}/systemd/system.conf.d/kubernetes-accounting.conf
%{_tmpfilesdir}/kubernetes.conf
%verify(not size mtime md5) %attr(755, kube,kube) %dir /run/%{name}

%files client
%{_bindir}/kubectl

%pre master
getent group kube >/dev/null || groupadd -r kube
getent passwd kube >/dev/null || useradd -r -g kube -d / -s /sbin/nologin -c \"Kubernetes user\" kube

%pre node
getent group kube >/dev/null || groupadd -r kube
getent passwd kube >/dev/null || useradd -r -g kube -d / -s /sbin/nologin -c \"Kubernetes user\" kube" > SPECS/kubernetes.spec

rpmbuild --define '_topdir '$(pwd) -ba SPECS/kubernetes.spec
