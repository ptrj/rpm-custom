#!/bin/bash

NAME=bmon
VERSION=4.0

# Clean all
for dir in BUILD BUILDROOT RPMS SOURCES SPECS SRPMS
do
  [[ -d $dir ]] && rm -Rf $dir
  mkdir -p $dir
done

echo "Name:           bmon
Version:        ${VERSION}
Release:        1%{?dist}
Summary:        Console interface bandwidth usage monitor

Vendor:         Thomas Graf
License:        BSD
URL:            https://github.com/tgraf/bmon/
Source:         https://github.com/tgraf/%{name}/releases/download/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:  libconfuse-devel
BuildRequires:  libnl3-devel
BuildRequires:  gettext
BuildRequires:  ncurses-devel
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
bmon is an interface bandwidth monitor.

%prep
%setup -q -n %{name}-%{version}
%build
./configure \
        --prefix=%{_sysconfdir}/bmon \
        --sbindir=/usr/sbin \
        --bindir=/usr/bin \
        --datarootdir=/usr \
        --sharedstatedir=/usr/share \
        --infodir=/usr/share/bman \
        --localedir=/usr/share/bman \
        --mandir=/usr/share/man \
        --docdir=/usr/share/doc \
        $*

%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR="%{buildroot}"

%clean
%{__rm} -rf %{buildroot}

%files
%define _unpackaged_files_terminate_build 0
%defattr(-, root, root, 0755)
%doc NEWS ChangeLog
%doc examples/bmon.conf
%doc %{_mandir}/man8/bmon.8.gz
%{_bindir}/bmon" > SPECS/bmon.spec

wget https://github.com/tgraf/$NAME/releases/download/v$VERSION/$NAME-$VERSION.tar.gz -O SOURCES/$NAME-$VERSION.tar.gz
rpmbuild --define '_topdir '$(pwd) -ba SPECS/bmon.spec
