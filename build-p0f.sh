#!/bin/bash

NAME=p0f
VERSION=3.09b

# Clean all
for dir in BUILD BUILDROOT RPMS SOURCES SPECS SRPMS
do
  [[ -d $dir ]] && rm -Rf $dir
  mkdir -p $dir
done

echo "diff -Nur p0f-3.06b.orig/build.sh p0f-3.06b/build.sh
--- p0f-3.06b.orig/build.sh     2012-09-29 22:51:20.000000000 -0600
+++ p0f-3.06b/build.sh  2013-12-02 21:06:18.758155005 -0700
@@ -1,4 +1,4 @@
-#!/bin/bash
+#!/bin/bash -x
 #
 # p0f - build script
 # ------------------
@@ -13,12 +13,12 @@

 test \"\$CC\" = \"\" && CC=\"gcc\"

-BASIC_CFLAGS=\"-Wall -Wno-format -I/usr/local/include/ \\
-              -I/opt/local/include/ -DVERSION=\\"\"\$VERSION\\"\" \$CFLAGS\"
+BASIC_CFLAGS=\"-Wall -Werror=format-security \\
+              -DVERSION=\\"\"\$VERSION\\"\" \$CFLAGS\"

-BASIC_LDFLAGS=\"-L/usr/local/lib/ -L/opt/local/lib \$LDFLAGS\"
+BASIC_LDFLAGS=\"\$LDFLAGS\"

-USE_CFLAGS=\"-fstack-protector-all -fPIE -D_FORTIFY_SOURCE=2 -g -ggdb \\
+USE_CFLAGS=\"-fPIE -g -ggdb \\
             \$BASIC_CFLAGS\"

 USE_LDFLAGS=\"-Wl,-z,relro -pie \$BASIC_LDFLAGS\"
@@ -81,8 +81,8 @@
 elif [ \"\$1\" = \"all\" -o \"\$1\" = \"\" ]; then

   echo \"[+] Configuring production build.\"
-  BASIC_CFLAGS=\"\$BASIC_CFLAGS -O3\"
-  USE_CFLAGS=\"\$USE_CFLAGS -O3\"
+  BASIC_CFLAGS=\"\$BASIC_CFLAGS -O2\"
+  USE_CFLAGS=\"\$USE_CFLAGS -O2\"

 elif [ \"\$1\" = \"debug\" ]; then
"> SOURCES/p0f-3.06b-compiler.patch

echo "Name:           p0f
Version:        3.09b
Release:        1%{?dist}
Summary:        Versatile passive OS fingerprinting tool
Group:          Applications/Internet

License:        LGPLv2+
Vendor:         Michal Zalewski (lcamtuf@coredump.cx)
URL:            http://lcamtuf.coredump.cx/p0f3/
Source:         http://lcamtuf.coredump.cx/%{name}3/releases/%{name}-%{version}.tgz
# Fix up build script to use proper flags
Patch1:         p0f-3.06b-compiler.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libpcap-devel

%description
P0f is a versatile passive OS fingerprinting tool. P0f can identify the
system on machines that talk thru or near your box. p0f will also check
masquerading and firewall presence, the distance to the remote system and its
uptime, other guy's network hookup (DSL, OC3, avian carriers) and his ISP.

%prep
%setup -q
%patch1 -p1

%build
make %{?_smp_mflags} CFLAGS=\"\$RPM_OPT_FLAGS -DFP_FILE=\\"\"%{_sysconfdir}/p0f/p0f.fp\\"\"\"

%install
rm -rf \$RPM_BUILD_ROOT
%{__mkdir_p} \$RPM_BUILD_ROOT%{_sbindir}
%{__mkdir_p} \$RPM_BUILD_ROOT%{_sysconfdir}/p0f
%{__cp} p0f \$RPM_BUILD_ROOT%{_sbindir}
%{__cp} p0f.fp \$RPM_BUILD_ROOT%{_sysconfdir}/p0f

# Build the tools
cd tools
make %{?_smp_mflags} CFLAGS=\"\$RPM_OPT_FLAGS\"

%{__cp} p0f-client p0f-sendsyn p0f-sendsyn6 \$RPM_BUILD_ROOT%{_sbindir}


%clean
rm -rf \$RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc docs/*
%doc tools/README-TOOLS
%{_sbindir}/*
%dir %{_sysconfdir}/p0f
%config %{_sysconfdir}/p0f/p0f.fp" > SPECS/p0f.spec

wget http://lcamtuf.coredump.cx/${NAME}3/releases/${NAME}-${VERSION}.tgz -O SOURCES/$NAME-$VERSION.tgz
rpmbuild --define '_topdir '$(pwd) -ba SPECS/p0f.spec
